<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    public function firms(){
    return $this->belongsTo('App\Firm','FirmID');
}
}
