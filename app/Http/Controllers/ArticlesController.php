<?php

namespace App\Http\Controllers;

use App\Book;
use App\Phone;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Validator;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $book = $test = DB::select("SELECT * FROM `Books` ORDER BY `Books`.`created_at` DESC");



        return view('site.book',array(
            'books'=>$book,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');


        $massages =[    //Формуємо свої ошибки повідомлень
            'required'=>'Поле :attribute обовязкове для заповнення',
            'email'=>'Поле :attribute повине бути email адресом',

        ];
        $validator = Validator::make($input,
            [
                'name'=>'required|max:255',
                'email'=>'required|email',
                'text'=>'required'
            ],$massages);

        if ($validator->fails()){

            return redirect()->route('article.index')->withErrors($validator)->withInput();
        }

        $data = $request->all();

        $books = new Book();

        $books->fill($data);
        if ($books->save()){
            return redirect('article')->with('status','Нові дані записані');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
