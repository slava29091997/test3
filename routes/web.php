<?php
Route::get('/',['uses'=>'IndexController@execute','as'=>'home']);

Route::resource('article','ArticlesController',[
    'parameters'=> [
        'articles'=>'alias'
    ]
]);

    Route::group(['prefix'=>'admin','middleware'=>'auth'],function (){
        Route::get('/',['uses'=>'Admin\BookController@index']);
        Route::resource('articless','Admin\BookController');
    });


Auth::routes();


